import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../_models/index';
import { UserService } from '../_services/index';

@Component({
    moduleId: module.id,
    templateUrl: 'user.component.html',
    styleUrls: ['user.component.css']
})

export class UserComponent implements OnInit
{
    currentUser: User;
    users: User[] = [];

    constructor(private userService: UserService, private _activatedRoute: ActivatedRoute) 
    {
        
    }

    ngOnInit(): void 
    {
        let userId: number = this._activatedRoute.snapshot.params['id'];
        this.userService.getById(userId).subscribe(userInformation => { this.currentUser = userInformation; });;
    }
}