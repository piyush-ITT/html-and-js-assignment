/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var getarray;
var flag;

  function storeForm()
  {
    var firstName = document.getElementById("first_name").value;
    var lastName = document.getElementById("last_name").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var phone = document.getElementById("phone").value;
    var gender = document.getElementById("gender").value;
    flag = validateForm(firstName, lastName, password, phone);
     if(flag === 1)
    {
        flag =0;
        return false;
    }
    flag = storeUserDetails(firstName,lastName,email,password,phone, gender);
    if(flag === 1)
    {    
        return false;
    }
    else
    {
    window.location="main.html";
    return false;
    }
  }
  function validateForm(firstName, lastName, password, phone)
  {
      var ck_fname = /^[A-Za-z0-9 ]{3,20}$/;
      var ck_lname = /^[A-Za-z0-9 ]{3,20}$/;
      var ck_password =  /^(?=.*[0-9])(?=.*[!@#$%^&*])[A-Za-z0-9!@#$%^&*()_]{6,20}$/;
      var ck_phone = /^[0-9]{8,25}$/;
      if (!ck_fname.test(firstName)) 
      {
          document.getElementById("msg").innerHTML="First name must be greater than 2";
          return 1;
      }
      if (!ck_lname.test(lastName)) 
      {
          document.getElementById("msg").innerHTML="Last name must be greater than 2";
          return 1;
      }
      if (!ck_password.test(password)) 
      {
          document.getElementById("msg").innerHTML="Password must include Uppercase, Lowercase,special char & length atleast 6";
          return 1;
      }
      if(!ck_phone.test(phone))
      {
          document.getElementById("msg").innerHTML="Phone number length must be greater than 7";
          return 1;
      }
      
  }

  function user(firstName,lastName,email,password,phone, gender)
  {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.phone = phone;
    this.gender = gender;
  }
  function storeUserDetails(firstName,lastName,email,password,phone,gender)
  {
      getarray = JSON.parse(localStorage.getItem(email));
      if(getarray!==null)
      {
         alert("email already exits try please different email");
         return 1;
      }
      else
      {
          var newUserDetails = new user(firstName,lastName,email,password,phone, gender);
          localStorage.setItem(email,JSON.stringify(newUserDetails));
      }
  }

  function retrieving()
  {
      
      var email = document.getElementById("email").value;
      var password = document.getElementById("password").value;
      getarray = JSON.parse(localStorage.getItem(email));
      if(getarray !== null)
      {
        if(email === getarray.email && password === getarray.password)
        {         
            alert("Logged in successfully");
            window.location="main.html";
            return false;
        }
        else
        {
          document.getElementById("msg").innerHTML="Invalid username/password!";
          return false;
        }
      }
      else
      {
          document.getElementById("msg").innerHTML="Invalid username/password!";
          return false;
      }
  }
  
function logOut()
{
    alert("Sucessfully logged out!");
    window.location="index.html";
    return false;
}


