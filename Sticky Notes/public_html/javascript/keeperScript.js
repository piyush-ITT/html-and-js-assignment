/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
"use strict";
var flag1 = 0;
var notes = new Array();
function sideMenu()
{
    listVisibility();
    if (flag1 === 0)
    {
        document.getElementById("sidenavigation").style.width = "20rem";
        setTimeout(listVisibility, 300);               
        flag1 = 1;
    }
    else
    {
        document.getElementById("sidenavigation").style.width = "0";
        document.getElementsByTagName("UL")[0].style.display = "none";
        flag1 = 0;
    }
}
/*
 * function for sidenavigation list visibility
 */
function listVisibility()
{
    if(flag1 === 0)
        document.getElementsByTagName("UL")[0].style.display = "none";
    else
        document.getElementsByTagName("UL")[0].style.display = "block";
}

function showTextBox()
{
    document.getElementById("textbox-style").style.display = "none";
    document.getElementById("note").style.display = "block";
}

function clearAllInformation()
{
    document.getElementById("uploaded-image").src = "";
    document.getElementById("note-content").innerHTML = "";
}

function saveInformation()
{
    var title;
    var data;
    data = document.getElementById("note-content").innerHTML;
    var dataReplaceSpace = data.replace(/&nbsp;/g, "");
    dataReplaceSpace = dataReplaceSpace.replace(/<div><br><\/div>/g, "");
    var dataTrim = dataReplaceSpace.trim();
    var date = new Date();
    title = document.getElementById("title").value;
    var titleTrim = title.trim();
    var imageData = document.getElementById("uploaded-image").src;
    var imageExtension = imageData.substr((imageData.lastIndexOf('.') + 1));
    var id = getUserId();
    if(titleTrim !== "") 
    {
        saveInLocalStorage(notes, new notesInformation(id, date, data, title, imageData));
    }
    else
    {
        if(dataTrim !== "")
        {
            saveInLocalStorage(notes, new notesInformation(id, date, data, title, imageData));
        }
        else
        {
            if(imageExtension !== "html")
            {
                saveInLocalStorage(notes, new notesInformation(id, date, data, title, imageData));
            }
            else
            {
                              
            }
        }
        
    }
    console.log("data saved");
    document.getElementById("textbox-style").style.display = "block";
    document.getElementById("note").style.display = "none";
    document.getElementById("note-content").innerHTML="";
    document.getElementById("title").value="";
    document.getElementById("uploaded-image").src = "";
}
function notesInformation(id, date, data, title, imageData)
{
    this.id = id;
    this.date = date;
    this.data = data;
    this.title = title;
    this.imageData = imageData;
}
function saveInLocalStorage(array, noteData)
{
    var array;
    if (localStorage.getItem("userInformation") === null)
    {
        array.push(noteData);
        localStorage.setItem("userInformation", JSON.stringify(array));
    }
    else
    {
        array = JSON.parse(localStorage.getItem("userInformation"));
        array.push(noteData);
        localStorage.setItem("userInformation", JSON.stringify(array));
    }
        setNotesLeftToRight();
    
}
/**
 * Function to display Notes
 */
var flag=3;
var count =0;
function setNotesLeftToRight()
{
    
    var array;
    var length;
    var oldNode, previousNode, latestNode;
    var tilesFooter;
    var child, subchild1, subchild2, subchild3, subchild4;
    array = JSON.parse(localStorage.getItem("userInformation"));
    length = array.length;
    tilesFooter = ['<div class="tiles-footer" id = "tiles-footer"><span class="glyphicon glyphicon-picture footer-hover"></label><input type="file" class = "forimage-upload" id="' + array[length - 1].id + '" onchange="uploadImage(this,this.id)"/></span><span class="glyphicon glyphicon-trash cursor-pointer footer-hover" id="' + array[length - 1].id + '" onclick="deleteNote(this.id)"></span><span class="fa fa-archive footer-hover" id="' + array[length - 1].id + '" onclick="archiveNote(this.id)"></span><span id = "date-display"></span></div>']; 
    oldNode = document.getElementById("old-save");
    previousNode = document.getElementById("previous-save");
    latestNode = document.getElementById("latest-save");
        child = document.createElement("div");
        subchild1 = document.createElement("div");
        subchild2 = document.createElement("div");
        subchild3 = document.createElement("div");
        subchild4 = document.createElement("img");
        child.className += "well insingle-line";  
        subchild1.innerHTML = array[length - 1].title;
        subchild2.innerHTML = array[length - 1].data;
        subchild3.innerHTML = tilesFooter;
        subchild4.src = array[length - 1].imageData;
        subchild1.className += "title-font";
        subchild2.className += "data-font";
        subchild3.className += "footer-font";
        subchild4.className += "img-responsive";
        subchild4.id = "image";
        child.id = "subchild1";
        child.appendChild(subchild4);
        child.appendChild(subchild1);
        child.appendChild(subchild2);
        child.appendChild(subchild3);
        
           var stringStore;
           stringStore = oldNode.innerHTML;
           if(flag === 3)
           {    
                oldNode.innerHTML = latestNode.innerHTML;
                flag =2;
           }
           latestNode.innerHTML = previousNode.innerHTML;
           previousNode.innerHTML = stringStore; 
           if(count !== 3)
           {
                if(length < 4)
                {
                    oldNode.innerHTML = "";
                    count++;
                }
           }
           else
           {
               count =3;
           }
           oldNode.insertBefore(child, oldNode.childNodes[0]);
           document.getElementById("old-save").innerHTML = "";
           document.getElementById("previous-save").innerHTML = "";
           document.getElementById("latest-save").innerHTML = "";
           renderData(0);
           flag++;
}

function getUserId()
{
    var array;
    if (localStorage.getItem("userInformation") === null)
    {
        return 1;
    }
    else
    {
        array = JSON.parse(localStorage.getItem("userInformation"));
        length = array.length;
        return array.length+1;
    }
}

function savingImage(input)
{
    document.getElementById("uploaded-image").style.display = "block";

    if (input.files && input.files[0])
    {
        var reader = new FileReader();

        reader.onload = function (e)
        {
            document.getElementById('uploaded-image').src = e.target.result;

        };

        reader.readAsDataURL(input.files[0]);
    }
}
function archiveNote(id)
{
    var array = JSON.parse(localStorage.getItem("userInformation"));
    var i;
    for (i = 0; i < array.length; i++)
    {
        if (array[i].id == id)
        {
            var trashArray = new Array();
            trashArray = new theTrashData(array[i].id, array[i].date,array[i].data, array[i].title, array[i].imageData);
            var array1 = new Array();
           if (localStorage.getItem("archiveInformation") === null)
           {
               array1.push(trashArray);
               localStorage.setItem("archiveInformation", JSON.stringify(array1));
           }
           else
           {
               array1 = JSON.parse(localStorage.getItem("archiveInformation"));
               array1.push(trashArray);
               localStorage.setItem("archiveInformation", JSON.stringify(array1));
           }
            array.splice(i, 1);
            break;
        }
    }
    localStorage.setItem("userInformation", JSON.stringify(array));
     $(document).ready(function(){
            $('#id').click(function()
           {
                   $("#myArchiveModale").modal();
           });
       });
    document.getElementById("old-save").innerHTML = "";
    document.getElementById("previous-save").innerHTML = "";
    document.getElementById("latest-save").innerHTML = "";
    renderData(0);
}
function deleteNote(id)
{
    var array = JSON.parse(localStorage.getItem("userInformation"));
    var i;
    for (i = 0; i < array.length; i++)
    {
        if (array[i].id == id)
        {
           var trashArray = new Array();
           trashArray = new theTrashData(array[i].id, array[i].date,array[i].data, array[i].title, array[i].imageData);
           var array1 = new Array();
           if (localStorage.getItem("trashInformation") === null)
           {
               array1.push(trashArray);
               localStorage.setItem("trashInformation", JSON.stringify(array1));
           }
           else
           {
               array1 = JSON.parse(localStorage.getItem("trashInformation"));
               array1.push(trashArray);
               localStorage.setItem("trashInformation", JSON.stringify(array1));
           }
            array.splice(i, 1);
            break;
        }      
    }
    localStorage.setItem("userInformation", JSON.stringify(array));
    document.getElementById("old-save").innerHTML = "";
    document.getElementById("previous-save").innerHTML = "";
    document.getElementById("latest-save").innerHTML = "";   
    $(document).ready(function()
    {
        $('#id').click(function()
       {
               $("#myModale").modal();
       });
    });
    
    renderData(0);
}

function theTrashData(id, date, data, title, imageData)
{
    this.id = id;
    this.date = date;
    this.data = data;
    this.title = title;
    this.imageData = imageData;
}

function renderData(number)
{
    var i;
    var array;
    var flag = 0;
    var first = document.getElementById("old-save");
    var second = document.getElementById("previous-save");
    var third = document.getElementById("latest-save");
    var tile;

    if (localStorage.getItem("userInformation") !== null)
    {
        array = JSON.parse(localStorage.getItem("userInformation"));
    }
    var length = array.length;
    for (i = length - 1 - number; i >= 0; i--)
    {
        var tilesFooter = ['<div class="tiles-footer"><label><span class="glyphicon glyphicon-picture footer-hover" title="add image">\n\
                            <input type="file" style="display:none" id="' + array[i].id + '" onchange="uploadImage(this,this.id)"/>\n\
                            </span></label><span class="glyphicon glyphicon-trash cursor-pointer footer-hover" data-toggle="modal" data-target="#myModale" title="delete note" id="' + array[i].id + '" onclick="deleteNote(this.id)"></span><span class="fa fa-archive footer-hover" style = "position:relative" data-toggle="modal" data-target="#myArchiveModale" title="archive note" id="' + array[i].id + '" onclick="archiveNote(this.id)"></span></div>'];
        tile = "<div id='draggable' style='decoration:none' class='well insingle-line ui-widget-content'><img id='image'"+array[i].id +" src='" + array[i].imageData + "' class='img-responsive'/><div class='title-font'>" + array[i].title +
                "</div><div class='data-font' id='" + array[i].id + "' data-toggle='modal' data-target='#myModal' onclick='editInformation(this.id)'>" + array[i].data + "</div><div class = 'date-style'>" + array[i].date.substring(0, 10) + "</div>" + tilesFooter + "</div>";
        if (flag === 0)
        {
            first.innerHTML += tile;
            flag = 1;
        }
        else
        if (flag === 1)
        {
            second.innerHTML += tile;
            flag = 2;
        }
        else
        {
            third.innerHTML += tile;
            flag = 0;
        }
    }

}
function ArchivePage()
{
    window.location="archive.html";
}

function TrashPage()
{
    window.location="trash.html";
}

/*
 * Method for changing the image
 */
function uploadImage(input,id)
{
    document.getElementById(id).style.display = "block";
    var array2 = new Array();
    array2 = JSON.parse(localStorage.getItem("userInformation"));
          
        if (input.files && input.files[0])
        {
            var reader = new FileReader();

            reader.onload = function (e)
            {
                 for (var i = 0; i < array2.length; i++)
                 { 
                    if(array2[i].id == id)
                    {
                        array2[i].imageData = e.target.result;                      
                    }
                 }
                 localStorage.setItem("userInformation",JSON.stringify(array2));
            };
            location.reload();
            reader.readAsDataURL(input.files[0]);
    }
}
/*
 * Method for getting information of notes
 */
function editInformation(id)
{
    var i, length;
    var array;
    array = JSON.parse(localStorage.getItem("userInformation"));
    length = array.length;

    for (i = 0; i < length; i++)
    {
        if (array[i].id == id)
        {
            document.getElementById("model-title").value = array[i].title;
            document.getElementById("model-note-content").innerHTML = array[i].data;
            document.getElementById("id-holder").value = array[i].id;
        }
    }
}
/**
 * Editing the Notes
 */
function storeModelData(id)
{
    var i, length, title, content;
    var array;
    array = JSON.parse(localStorage.getItem("userInformation"));
    length = array.length;
    title = document.getElementById("model-title").value;
    content = document.getElementById("model-note-content").innerHTML;
    for (i = 0; i < length; i++)
    {
        if (array[i].id == id)
        {
            array[i].title = title;
            array[i].data = content;
        }
    }
    localStorage.setItem("userInformation", JSON.stringify(array));
    document.getElementById("old-save").innerHTML = "";   
    document.getElementById("previous-save").innerHTML = "";
    document.getElementById("latest-save").innerHTML = "";
    renderData(0);
}

 $( function() {
    $( "#old-save" ).sortable({
        revert: true
    });
  } );
  $( function() {
    $( "#previous-save" ).sortable({
        revert: true
    });
  } );
  $( function() {
    $( "#latest-save" ).sortable({
        revert: true
    });
  } );
