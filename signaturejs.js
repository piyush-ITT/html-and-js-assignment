function init()
{
        var canvas = document.getElementById('signature');
        var context = canvas.getContext('2d');
        var isDrawing;

        canvas.onmousedown = function(e)
        {
          isDrawing = true;
          context.lineWidth = 1;
          context.lineJoin = context.lineCap = 'round';
          context.moveTo(e.clientX, e.clientY);
        };
        canvas.onmousemove = function(e)
        {
          if (isDrawing)
          {
            context.lineTo(e.clientX, e.clientY);
            context.stroke();
          }
        };
        canvas.onmouseup = function() {
          isDrawing = false;
        };


}
// function for clear screen
 function clea()
    {

      var canvas = document.getElementById('signature');
      var context = canvas.getContext('2d');
      context.clearRect(0, 0, canvas.width, canvas.height);
      location.reload();
    }

// function for submit    
function submi()
    {
      var canvas = document.getElementById('signature');
      var context = canvas.getContext('2d');
      var sign = canvas.toDataURL();
      localStorage.setItem('signature', sign);
      alert("add Successfully");

    }
